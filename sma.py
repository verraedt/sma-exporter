#!/usr/bin/python3

from prometheus_client import start_http_server, Gauge
import requests
import json
import time
import os
import logging
import urllib3

urllib3.disable_warnings()

logging.basicConfig(level=logging.INFO)

class SMAError(Exception):
    def __init__(self, rc, phase):
        self.rc = rc
        self.phase = phase
        super(SMAError, self).__init__('Error %d while %s' % (rc, phase))

class NoSessionError(Exception):
    def __init__(self):
        super(NoSessionError, self).__init__('No free sessions')

class SMA(object):

    TAGS = {
        # DC side
        '6380_40251E00': ('solar_power', 'W'),
        '6100_00411E00': ('solar_power_maximum', 'W'),
        
        # AC side
        '6100_00465700': ('ac_frequency', 'Hz', 100),
        '6100_00464800': ('ac_voltage_l1', 'V', 100),
        '6100_00464900': ('ac_voltage_l2', 'V', 100),
        '6100_00464A00': ('ac_voltage_l3', 'V', 100),
        '6380_40451F00': ('ac_voltage', 'V', 100),
        '6100_40465300': ('ac_current_l1', 'A', 1000),
        '6100_40465400': ('ac_current_l2', 'A', 1000),
        '6100_40465500': ('ac_current_l3', 'A', 1000),
        '6380_40452100': ('ac_current', 'A', 1000),
        '6100_40464000': ('ac_power_l1', 'W'),
        '6100_40464100': ('ac_power_l2', 'W'),
        '6100_40464200': ('ac_power_l3', 'W'),
        '6100_40263F00': ('ac_power', 'W'),
        '6100_0046C200': ('ac_power', 'W'),
        
        # AC side - meter
        '6400_0046C300': ('ac_gen_meter', 'kWh', 1000),
        '6400_00260100': ('total_yield', 'kWh', 1000),
        '6400_00262200': ('daily_yield', 'kWh', 1000),

        # AC side - grid measurements
        '6100_40463600': ('grid_power_supplied', 'W'),
        '6100_40463700': ('grid_power_absorbed', 'W'),
        '6400_00462400': ('grid_total_yield', 'kWh', 1000),
        '6400_00462500': ('grid_total_absorbed', 'kWh', 1000),
        '6100_00543100': ('grid_current_consumption', 'W'),
        '6400_00543A00': ('grid_total_consumption', 'kWh', 1000),
        
        # Time
        '6400_00462E00': ('service_time', 's'),
        '6400_00462F00': ('injection_time', 's'),

        # Server settings
        '6180_104A9A00': ('server_ip', ),
        '6180_104A9D00': ('server_dns', ),
        '6180_104A9B00': ('server_netmask', ),
        '6180_104A9C00': ('server_gateway', ),
        '6180_084A9600': ('ethernet_status', ),
        '6180_084AAA00': ('ethernet_counter', ),

        # WLAN settings
        '6100_004AB600': ('wlan_strength', ),
        '6180_104AB700': ('wlan_ip', ),
        '6180_104AB800': ('wlan_netmask', ),
        '6180_104AB900': ('wlan_gateway', ),
        '6180_104ABA00': ('wlan_dns', ),
        '6180_084ABC00': ('wlan_status', ),
        '6180_084ABB00': ('wlan_scan_status', ),

        # Device status
        '6180_084B1E00': ('device_state', ),
        '6100_00411F00': ('device_warnings', ),
        '6100_00412000': ('device_errors', ),
    }

    DEFAULT = [
        '6380_40251E00',
        '6100_00465700',
        '6100_00464800',
        '6380_40451F00',
        '6100_40465300',
        '6380_40452100',
        '6100_40464000',
        '6100_0046C200',
        '6400_0046C300',
        '6400_00260100',
        '6400_00262200',
        '6400_00462E00',
        '6400_00462F00',
        '6100_00411F00',
        '6100_00412000',
    ]

    THREEPHASES = [
        '6100_00464900',
        '6100_00464A00',
        '6100_40465400',
        '6100_40465500',
        '6100_40464100',
        '6100_40464200',
    ]

    def __init__(self, url, right = 'usr', password = '0000'):
        self.url = url
        self.right = right
        self.password = password
        self._sid = None

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.__logout()

    def __login(self):
        r = requests.post('%s/dyn/login.json' % self.url, json={
          'right': self.right,
          'pass': self.password,
        }, verify=False)
        r.raise_for_status()
        data = r.json()
        if 'err' in data:
            return data['err']
        self._sid = data['result']['sid']
        return 200

    def __logout(self):
        r = requests.post('%s/dyn/logout.json?sid=%s' % (self.url, self._sid), json={}, verify=False)
        r.raise_for_status()
        data = r.json()
        if 'err' in data:
            return data['err']
        if data['result']['isLogin']:
            return 500
        self._sid = None
        return 200

    def __check(self):
        if self._sid is None:
            url = '%s/dyn/sessionCheck.json' % self.url
        else:
            url = '%s/dyn/sessionCheck.json?sid=%s' % (self.url, self._sid)
        r = requests.post(url, json={}, verify=False)
        r.raise_for_status()
        data = r.json()
        if 'err' in data:
            return data['err'], {}
        return 200, {
            'valid': 'cntDwnGg' in data['result'],
            'freeSessions': data['result']['cntFreeSess'],
        }

    def __read(self, keys):
        if isinstance(keys, str):
            r = requests.post('%s/dyn/%s.json?sid=%s' % (self.url, keys, self._sid), json={'destDev': []}, verify=False)
        else:
            r = requests.post('%s/dyn/getValues.json?sid=%s' % (self.url, self._sid), json={'destDev': [], 'keys': keys}, verify=False)
        r.raise_for_status()
        data = r.json()
        if 'err' in data:
            return data['err'], {}
        return 200, self.__parse(data)

    def __parse(self, data):
        result = {}
        for serial, values in data['result'].items():
            result[serial] = {}
            for tag, value in values.items():
                index = '0' if tag == '6800_10841E00' else '1'
                result[serial][tag] = value[index][0]
                if tag in SMA.TAGS:
                    info = SMA.TAGS[tag]
                    result[serial][tag]['label'] = info[0]
                    if len(info) > 1:
                        result[serial][tag]['unit'] = info[1]
                    if len(info) > 2:
                        for k in ['val', 'low', 'high']:
                            if k in result[serial][tag] and result[serial][tag][k] is not None:
                                result[serial][tag][k] /= info[2]
        return result

    def readAllValues(self):
        return self.read('getAllOnlValues')

    def readAllParams(self):
        return self.read('getAllParamValues')

    def read(self, keys = DEFAULT):
        # Check for valid session
        rc, status = self.__check()
        if rc != 200:
            raise SMAError(rc, 'check')

        if not status['valid']:
            if status['freeSessions'] <= 0:
                raise NoSessionError

            rc = self.__login()
            if rc != 200:
                raise SMAError(rc, 'login')

        # Read results
        rc, results = self.__read(keys)
        if rc == 401:
            # Session expired in the meantime

            self.__logout() # to be sure
            rc = self.__login()
            if rc != 200:
                raise SMAError(rc, 're-login')

            rc, results = self.__read(keys)

        if rc != 200:
            raise SMAError(rc, 'read')

        return results

if __name__ == "__main__":
    # Configuration
    url = os.getenv('SMA_URL', 'http://192.168.12.3')
    role = os.getenv('SMA_ROLE', 'usr') # or istl
    password = os.getenv('SMA_PASSWORD', '0000')
    refresh_interval = int(os.getenv('REFRESH_INTERVAL', '58'))
    max_retries = int(os.getenv('MAX_RETRIES', '3'))
    fail_timeout = int(os.getenv('FAIL_TIMEOUT', '3600'))

    keys = SMA.DEFAULT
    if os.getenv('SMA_PHASES', '1') == '3':
        keys += SMA.THREEPHASES

    # Collect gauges
    gauges = {}
    for tag in keys:
        name = SMA.TAGS[tag][0]
        description = name.replace('_', ' ')
        if len(SMA.TAGS[tag]) > 1:
            description += ' (%s)' % SMA.TAGS[tag][1]
        gauges[tag] = Gauge('sma_%s' % name, description, ['inverter'])

    # Start prometheus
    start_http_server(9000)

    # Main loop
    while True:
        retries = max_retries
        while retries > 0:
            # Collect metrics
            try:
                with SMA(url, role, password) as sma:
                    for inverter, values in sma.read(keys).items():
                        for tag, data in values.items():
                            if 'val' in data and data['val'] is not None:
                                logging.debug('Set tag %s to %f' % (tag, data['val']))
                                gauges[tag].labels(inverter).set(data['val'])
                            else:
                                logging.debug('Reset tag %s to zero' % tag)
                                gauges[tag].labels(inverter).set(0)
                break
            except SMAError as e:
                print(e)
                retries-= 1
            except NoSessionError as e:
                print(e)
                retries = 0

        if retries > 0:
            time.sleep(refresh_interval)
        else:
            logging.info('Sleeping %d seconds' % fail_timeout)
            time.sleep(fail_timeout)
