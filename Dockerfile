FROM python:3

WORKDIR /usr/src/app
RUN pip install --no-cache-dir prometheus_client requests

EXPOSE 9000

COPY ./sma.py /usr/src/app

CMD ["python", "sma.py"]
